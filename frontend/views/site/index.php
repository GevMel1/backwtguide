<?php

/** @var yii\web\View $this */

$this->title = 'WTGuide';
?>
<main class="page-main-index">
    <section class="page-main__main-section main-section">
        <div class="container">
            <h1 class="main-section__title">WTGUIDE</h1>
        </div>
    </section>
    <section class="page-main__desc-section desc-section">
        <div class="container">
            <div class="desc-section__wrapper">
                <img src="img/index/handbook.png" alt="handbook">
                <p class="desc-section__text text">WTGUIDE - это веб-приложение, созданное с целью упросить интеграцию новичка в
                    игру War Thunder. Игра перенасыщена различными механиками, которые усваиваются через большое количество часов,
                    а игроки часто выбирают неправильную тактику, теряя эффективность в бою. Если вы желаете повысить свои навыки,
                    то приглашаем в наш проект!</p>
            </div>
        </div>
    </section>
    <section class="page-main__down-section down-section">
        <div class="container">
            <div class="down-section__wrapper">
                <div class="down-section__wrapper-text">
                    <p class="down-section__label-text label-text">Внимание!</p>
                    <p class="down-section__text text">Вы должны пройти тест для формирования структуры обучения, а также выдачи
                        соответствующей роли. Тест можно пройти один раз на аккаунт, так что будьте внимательны!</p>
                </div>
                <nav class="down-section__down-nav down-nav">
                    <div class="down-nav__down-nav-item down-nav-item">
                        <img class="down-nav-item__img" src="img/index/down_nav/wiki.svg" alt="">
                        <p class="down-nav-item__down-nav-label down-nav-label">Wiki</p>
                    </div>
                    <div class="down-nav__down-nav-item down-nav-item">
                        <img class="down-nav-item__img" src="img/index/down_nav/forum.png" alt="">
                        <p class="down-nav-item__down-nav-label down-nav-label">Форум</p>
                    </div>
                    <div class="down-nav__down-nav-item down-nav-item">
                        <img class="down-nav-item__img" src="img/index/down_nav/learning.png" alt="">
                        <p class="down-nav-item__down-nav-label down-nav-label">Обучение</p>
                    </div>
                </nav>
            </div>
        </div>
    </section>
</main>
