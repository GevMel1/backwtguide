<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%post}}`.
 */
class m231212_082251_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{post}}', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'views' => $this->integer()->notNull()->defaultValue(0),
            'title' => $this->string(255)->notNull(),
            'content' => $this->string(5000)->notNull(),
            'numbers_like' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey(
            'fk-post-id_user',
            'post',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%post}}');
    }
}
