<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filtres_tanks}}`.
 */
class m231204_110440_create_filtres_tanks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%filtres_tanks}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
            'county' => $this->string(255)->notNull(),
            'rank' => $this->integer(1)->notNull(),
            'class' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%filtres_tanks}}');
    }
}
