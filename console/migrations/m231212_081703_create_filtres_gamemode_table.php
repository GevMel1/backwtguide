<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filtres_gamemode}}`.
 */
class m231212_081703_create_filtres_gamemode_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%filtres_gamemode}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%filtres_gamemode}}');
    }
}
