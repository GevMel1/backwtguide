<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sub_comments}}`.
 */
class m231212_082558_create_sub_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{sub_comments}}', [
            'id' => $this->primaryKey(),
            'id_comments' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'content' => $this->string(1000)->notNull(),
            'numbers_like' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey(
            'fk-sub_comments-id_comments',
            'sub_comments',
            'id_comments',
            'comments',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-sub_comments-id_user',
            'sub_comments',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sub_comments}}');
    }
}
