<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comments}}`.
 */
class m231212_082313_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{comments}}', [
            'id' => $this->primaryKey(),
            'id_post' => $this->integer()->notNull(),
            'id_user' => $this->integer()->notNull(),
            'content' => $this->string(1000)->notNull(),
            'numbers_like' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey(
            'fk-comments-id_user',
            'comments',
            'id_user',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%comments}}');
    }
}
