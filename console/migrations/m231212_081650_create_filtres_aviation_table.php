<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filtres_aviation}}`.
 */
class m231212_081650_create_filtres_aviation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%filtres_aviation}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
            'county' => $this->string(255)->notNull(),
            'rank' => $this->integer(1)->notNull(),
            'class' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%filtres_aviation}}');
    }
}
