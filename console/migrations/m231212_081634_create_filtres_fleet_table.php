<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filtres_fleet}}`.
 */
class m231212_081634_create_filtres_fleet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%filtres_fleet}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
            'county' => $this->string(255)->notNull(),
            'rank' => $this->integer(1)->notNull(),
            'class' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%filtres_fleet}}');
    }
}
